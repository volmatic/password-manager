<?php

include 'inc/config.php';
include 'inc/classes/Encryption.inc.php';
include 'inc/classes/Database.inc.php';
include 'inc/classes/Remote.inc.php';
include 'inc/classes/User.inc.php';
include 'inc/classes/Passwords.inc.php';
include 'inc/classes/Notes.inc.php';
include 'inc/classes/Keys.inc.php';

Database::Connect();
if (isset($_GET['a']) && isset($_GET['k'])) {
    //Always check for the key
    if ($_GET['k'] != $GLOBALS['app_key']) {
        echo json_encode(['status' => 'INVALID_USER']);
        die();
    }

    if ($_GET['a'] == 'status') {
        echo json_encode(['status' => Remote::GetStatus($_GET['k'])]);
        die();
    }

    //Key is valid, listen for other stuff
    if ($_GET['a'] == 'create' && isset($_GET['u']) && isset($_GET['p'])) {
        echo json_encode(User::Create($_GET['u'], $_GET['p']));
        die();
    }

    if ($_GET['a'] == 'auth' && isset($_GET['u']) && isset($_GET['p'])) {
        echo json_encode(User::Authenticate($_GET['u'], $_GET['p']));
        die();
    }

    if ($_GET['a'] == 'new_password' && isset($_GET['p']) && isset($_GET['owner']) && isset($_GET['name']) && isset($_GET['desc']) && isset($_GET['value'])) {
        Passwords::CreatePassword($_GET['owner'], $_GET['p'], $_GET['name'], $_GET['desc'], $_GET['value']);
        die();
    }

    if ($_GET['a'] == 'delete_password' && isset($_GET['p']) && isset($_GET['owner']) && isset($_GET['pw'])) {
        Passwords::DeletePassword($_GET['owner'], $_GET['p'], $_GET['pw']);
        die();
    }

    if ($_GET['a'] == 'edit_password' && isset($_GET['p']) && isset($_GET['owner']) && isset($_GET['id']) && isset($_GET['new'])) {
        echo json_encode(Passwords::ChangePassword($_GET['owner'], $_GET['p'], $_GET['id'], $_GET['new']));
        die();
    }

    if ($_GET['a'] == 'password_list' && isset($_GET['uid']) && isset($_GET['p'])) {
        echo json_encode(Passwords::GetPasswords($_GET['uid'], $_GET['p']));
        die();
    }

    if ($_GET['a'] == 'note_list' && isset($_GET['uid']) && isset($_GET['p'])) {
        echo json_encode(Notes::GetNotes($_GET['uid'], $_GET['p']));
        die();
    }

    if ($_GET['a'] == 'new_note' && isset($_GET['p']) && isset($_GET['owner']) && isset($_GET['name'])) {
        Notes::CreateNote($_GET['owner'], $_GET['p'], $_GET['name']);
        die();
    }

    if ($_GET['a'] == 'delete_note' && isset($_GET['p']) && isset($_GET['owner']) && isset($_GET['noteID'])) {
        Notes::DeleteNote($_GET['owner'], $_GET['p'], $_GET['noteID']);
        die();
    }

    if ($_GET['a'] == 'note_content' && isset($_GET['uid']) && isset($_GET['p']) && isset($_GET['noteID'])) {
        echo json_encode(Notes::GetNoteContent($_GET['uid'], $_GET['p'], $_GET['noteID']));
        die();
    }

    if ($_GET['a'] == 'update_note' && isset($_GET['p']) && isset($_GET['owner']) && isset($_GET['noteID']) && isset($_GET['content'])) {
        Notes::UpdateNote($_GET['owner'], $_GET['p'], $_GET['noteID'], $_GET['content']);
        die();
    }

    if ($_GET['a'] == 'key_list' && isset($_GET['uid']) && isset($_GET['p'])) {
        echo json_encode(Keys::GetKeys($_GET['uid'], $_GET['p']));
        die();
    }

    if ($_GET['a'] == 'new_key' && isset($_GET['p']) && isset($_GET['owner']) && isset($_GET['name']) && isset($_GET['content']) && isset($_GET['pass'])) {
        Keys::CreateKey($_GET['owner'], $_GET['p'], $_GET['name'], $_GET['content'], $_GET['pass']);
        die();
    }

    if ($_GET['a'] == 'delete_key' && isset($_GET['p']) && isset($_GET['owner']) && isset($_GET['id'])) {
        Keys::DeleteKey($_GET['owner'], $_GET['p'], $_GET['id']);
        die();
    }
} else {
    die();
}
