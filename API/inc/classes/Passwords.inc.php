<?php

class Passwords
{
    public function GetPasswords($uid, $password)
    {
        $answer = '';
        if (User::B_AuthByUID($uid, $password)) {
            $queryP = $GLOBALS['db']->prepare('SELECT * FROM passwords WHERE owner=:owner');
            $queryP->execute([
                ':owner' => $uid
            ]);

            while ($row = $queryP->fetch(PDO::FETCH_OBJ)) {
                $answer .= '<<<|<<!<<<!>>>>!>|>>'.$row->id.'<o!fSDnmfbnfbds97fgb|!*Ç>'.$row->name.'<o!fSDnmfbnfbds97fgb|!*Ç>'.$row->comments.'<o!fSDnmfbnfbds97fgb|!*Ç>'.$row->value;
            }
        }

        return [
            'code' => 'OK',
            'data' => $answer
        ];
    }

    public function CreatePassword($uid, $password, $name, $desc, $value)
    {
        if (User::B_AuthByUID($uid, $password)) {
            if (mb_strlen($value) >= 5 && mb_strlen($name) > 3) {
                $date = new DateTime();
                $now = $date->getTimestamp();
                $query = $GLOBALS['db']->prepare('INSERT INTO passwords (owner,name,value,last_modified,created_at,comments) VALUES (:uid,:name,:password,:current_time,:created_at,:desc)');
                $query->execute([
                    ':uid' => $uid,
                    ':name' => $name,
                    ':password' => $value,
                    ':current_time' => $now,
                    ':created_at' => $now,
                    ':desc' => $desc
                ]);
            }
        }
    }

    public function ChangePassword($uid, $password, $id, $new)
    {
        if (User::B_AuthByUID($uid, $password)) {
            $query = $GLOBALS['db']->prepare('UPDATE passwords SET `value`=:value WHERE `id`=:id AND owner=:owner LIMIT 1');
            $query->execute([
                ':value' => $new,
                ':id' => $id,
                ':owner' => $uid
            ]);

            if ($query) {
                return [
                    'code' => 'OK'
                ];
            }
        }

        return [
            'code' => 'ERROR'
        ];
    }

    public function DeletePassword($uid, $password, $id)
    {
        if (User::B_AuthByUID($uid, $password)) {
            $query = $GLOBALS['db']->prepare('DELETE FROM passwords WHERE `id`=:id AND owner=:owner LIMIT 1');
            $query->execute([
                ':id' => $id,
                ':owner' => $uid
            ]);
        }
    }
}
