<?php

class Notes
{
    public function GetNotes($uid, $password)
    {
        $answer = '';
        if (User::B_AuthByUID($uid, $password)) {
            $queryP = $GLOBALS['db']->prepare('SELECT * FROM notes WHERE owner=:owner');
            $queryP->execute([
                ':owner' => $uid
            ]);

            while ($row = $queryP->fetch(PDO::FETCH_OBJ)) {
                $answer .= '<<<|<<!<<<!>>>>!>|>>'.$row->id.'<o!fSDnmfbnfbds97fgb|!*Ç>'.$row->name.'<o!fSDnmfbnfbds97fgb|!*Ç>'.$row->value;
            }
        }

        return [
            'code' => 'OK',
            'data' => $answer
        ];
    }

    public function UpdateNote($uid, $password, $note, $content)
    {
        if (User::B_AuthByUID($uid, $password)) {
            $query = $GLOBALS['db']->prepare('UPDATE notes SET `value`=:content WHERE owner=:owner AND `id`=:id LIMIT 1');
            $query->execute([
                ':content' => $content,
                ':owner' => $uid,
                ':id' => $note
            ]);
        }
    }

    public function GetNoteContent($uid, $password, $noteID)
    {
        $answer = '';
        if (User::B_AuthByUID($uid, $password)) {
            $queryP = $GLOBALS['db']->prepare('SELECT value FROM notes WHERE owner=:owner AND `id`=:id LIMIT 1');
            $queryP->execute([
                ':owner' => $uid,
                ':id' => $noteID
            ]);
            $row = $queryP->fetch(PDO::FETCH_OBJ);
            $answer = $row->value;
        }

        return [
            'code' => 'OK',
            'data' => $answer
        ];
    }

    public function CreateNote($uid, $password, $name)
    {
        if (User::B_AuthByUID($uid, $password)) {
            $query = $GLOBALS['db']->prepare('INSERT INTO notes (`owner`,`name`) VALUES (:owner, :name)');
            $query->execute([
                ':owner' => $uid,
                ':name' => $name
            ]);
        }
    }

    public function DeleteNote($uid, $password, $noteID)
    {
        if (User::B_AuthByUID($uid, $password)) {
            $query = $GLOBALS['db']->prepare('DELETE FROM notes WHERE `id`=:id AND owner=:owner LIMIT 1');
            $query->execute([
                ':id' => $noteID,
                ':owner' => $uid
            ]);
        }
    }
}
