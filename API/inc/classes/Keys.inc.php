<?php
class Keys
{
    public function GetKeys($uid, $password)
    {
        $answer = '';
        if (User::B_AuthByUID($uid, $password)) {
            $queryP = $GLOBALS['db']->prepare('SELECT * FROM `keys` WHERE owner=:owner');
            $queryP->execute([
                ':owner' => $uid
            ]);

            while ($row = $queryP->fetch(PDO::FETCH_OBJ)) {
                $answer .= '<<<|<<!<<<!>>>>!>|>>'.$row->id.'<o!fSDnmfbnfbds97fgb|!*Ç>'.$row->name.'<o!fSDnmfbnfbds97fgb|!*Ç>'.$row->content.'<o!fSDnmfbnfbds97fgb|!*Ç>'.$row->passphrase;
            }
        }

        return [
            'code' => 'OK',
            'data' => $answer
        ];
    }

    public function CreateKey($uid, $password, $name, $key, $pass)
    {
        if (User::B_AuthByUID($uid, $password)) {
            $query = $GLOBALS['db']->prepare('INSERT INTO `keys` (`owner`,`name`,`content`,`passphrase`) VALUES (:owner, :name, :content, :passphrase)');
            $query->execute([
                ':owner' => $uid,
                ':name' => $name,
                ':content' => $key,
                ':passphrase' => $pass
            ]);
        }
    }

    public function DeleteKey($uid, $password, $id)
    {
        if (User::B_AuthByUID($uid, $password)) {
            $query = $GLOBALS['db']->prepare('DELETE FROM `keys` WHERE `id`=:id AND owner=:owner LIMIT 1');
            $query->execute([
                ':id' => $id,
                ':owner' => $uid
            ]);
        }
    }
}
