<?php

class Database
{
    public static function Connect()
    {
        try {
            $GLOBALS['db'] = new PDO('mysql:host='.$GLOBALS['db_host'].';dbname='.$GLOBALS['db_name'].';', $GLOBALS['db_user'], $GLOBALS['db_password']);
        } catch (PDOException $e) {
            die('Fatal error, please contact a web admin.');
        }
    }
}
