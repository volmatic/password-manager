<?php

class User
{
    public function Create($username, $password)
    {
        if (mb_strlen($username) >= 4 && mb_strlen($password) >= 5) {
            $date = new DateTime();
            $time = $date->getTimestamp();
            $options = ['cost' =>  12];

            $username = Encryption::decryptRJ256($username);
            $password = Encryption::decryptRJ256($password);

            $query = $GLOBALS['db']->prepare('INSERT INTO users (`username`,`password`,`create_time`) VALUES (:username, :password, :create_time)');
            $query = $query->execute([
                ':username' => $username,
                ':password' => password_hash($password, PASSWORD_BCRYPT, $options),
                ':create_time' => $time
            ]);
            if ($query) {
                return [
                    'code' => 'OK'
                ];
            }
            return [
                    'code' => 'ERROR'
                ];
        }
        return [
                'code' => 'ERROR'
            ];
    }

    public static function B_AuthByUID($uid, $password)
    {
        $query = $GLOBALS['db']->prepare('SELECT * FROM users WHERE `id`=:uid LIMIT 1');
        $query->execute([':uid' => $uid]);
        $row = $query->fetch(PDO::FETCH_OBJ);
        if ($row->password != '') {
            $options = ['cost' => 12];
            $password = Encryption::decryptRJ256($password);
            $hashed_password = password_hash($password, PASSWORD_BCRYPT, $options);
            if (password_verify($password, $hashed_password)) {
                return true;
            }
        }
        return false;
    }

    public function Authenticate($username, $password)
    {
        $username = Encryption::decryptRJ256($username);
        $password = Encryption::decryptRJ256($password);
        $query = $GLOBALS['db']->prepare('SELECT * FROM users WHERE `username`=:username LIMIT 1');
        $query->execute([':username' => $username]);
        $row = $query->fetch(PDO::FETCH_OBJ);
        if ($row->password != '') {
            $options = ['cost' => 12];
            $hashed_password = password_hash($password, PASSWORD_BCRYPT, $options);
            if (password_verify($password, $hashed_password)) {
                $date = new DateTime();
                $time = $date->getTimestamp();

                return [
                    'code' => 'OK',
                    'uid' => $row->id,
                    'timestamp' => $time,
                    'username' => $row->username
                ];
            }
        }

        return [
            'code' => 'ERROR',
            'timestamp' => 0,
            'username' => ''
        ];
    }
}
