# Password Manager

Password Manager is a configurable and open source manager for passwords, notes and keys.

- Complete encryption both in server and client
- Easy to use and not intrusive, it's just a password manager after all *cough* dashlane sucks *cough*
- Completely free
- Access it locally or set it up in a remote server, it's up to you

### Installation

You can download the latest release or build it yourself (.NET 4.5.2 or newer).

*Since commit 6422c9a58320dc6380717edb64bb1e2c76830d1f you no longer need to do the following steps* 

Once you have it, you need to configure a few things in the file named ```Config``` inside the root directory.

It should look something like this:

```json
{
	"local":false,
	"api_url":"https://apis.volmatic.xyz/pm/",
	"max_tries":"3",
	"web_key":"",
	"key":"", //128 bits
	"vectors":"" //128 bits
}
```

```local```         <- Set it to true if you want to use it locally. If this is set to true you can skip ```api_url, max_tries and web_key```

```api_url```       <- The URL of the API (Use the default one or continue reading on how to host it yourself)

```max_tries```     <- Stop trying to connect to the specified API after x tries.

```web_key```       <- A arbitrary key that is matched in the API

```key```           <- Key used to encrypt and decrypt data sent/received (128 Bit Length)

```vectors```       <- IV used to encrypt and decrypt data sent/received (128 Bit Length)

If everything is correct, you should be able to now use it.

### Hosting the API

If you want, you can host the api yourself. The API contents are inside the API Folder in this repo.

Requirements:

PHP version:``` 7.2 or newer ```

PHP Extensions: ```openssl, pdo, mysql, mysqli, gd, cli, mbstring, xml, json, common, pecl, intl, jsonlint, mysqlnd ```

```MySQL Server``` - You can find the database structure in API/Structure.sql

### Configuring the API

Go to API/inc/config.php and configure the database configs as well as the app configs.
The app_key, key and vectors correspond to the web_key, key and vectors in the App configuration respectively

Alternatively, you can use ```https://apis.volmatic.xyz/pm/``` as the API

### License

MIT License

Copyright (c) 2019 Volmatic

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.