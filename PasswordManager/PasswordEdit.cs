﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PasswordManager
{
    public partial class PasswordEdit : Form
    {
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        private PasswordPage pp;

        public PasswordEdit(PasswordPage p)
        {
            InitializeComponent();
            pp = p;
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            Globals.CURRENT_EDIT_PASSWORD_ID = -1;
            this.Hide();
        }
        
        private void PasswordEdit_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            if(confirmBox.Text == newBox.Text)
            {
                if(Requests.ChangePassword(newBox.Text))
                {
                    pp.passwords[Globals.CURRENT_EDIT_PASSWORD_NUM].Value = newBox.Text;
                    Globals.CURRENT_EDIT_PASSWORD_ID = -1;
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Something went wrong, please check your fields and try again.");
                }
            }
            else
            {
                MessageBox.Show("The new passwords don't match");
            }
        }
    }
}
