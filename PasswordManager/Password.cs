﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PasswordManager
{
    public class Password
    {
        public Password(int id, string name, string desc,string value)
        {
            this.ID = id;
            this.Name = name;
            this.Description = desc;
            this.Value = value;
        }
        
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Value { get; set; }
    }
    
}
