﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;

namespace PasswordManager
{
    public partial class KeysPage : UserControl
    {
        private static KeysPage Instance;

        public KeysPage()
        {
            InitializeComponent();
            Instance = this;
        }

        private Dictionary<int, Key> keys = new Dictionary<int, Key>();
        public static void UpdateKeysList()
        {
            int c = 0;
            Instance.keys.Clear();
            Instance.keyBindingSource.Clear();
            string dataRaw = Requests.GetKeys();
            string[] dataArray = dataRaw.Split(new string[] { "<<<|<<!<<<!>>>>!>|>>" }, StringSplitOptions.None);

            foreach (string data in dataArray)
            {
                if (data != "")
                {
                    string[] d0 = data.Split(new string[] { "<o!fSDnmfbnfbds97fgb|!*Ç>" }, StringSplitOptions.None);
                    if (d0.Length != 0)
                    {
                        _Cfg cfg = new _Cfg();
                        int keyId = Int32.Parse(d0[0]);
                        string name = Encryption.Decrypt(d0[1], cfg.KEY, cfg.VECTORS);
                        string key = Encryption.Decrypt(d0[2], cfg.KEY, cfg.VECTORS);
                        string pass = Encryption.Decrypt(d0[3], cfg.KEY, cfg.VECTORS);

                        Key _key = new Key(keyId, name, key, pass);
                        Instance.keys.Add(c, _key);
                        Instance.keyBindingSource.Add(_key);
                        c++;
                    }
                }
            }
        }

        private void KeysPage_Load(object sender, EventArgs e)
        {
            KeysGrid.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(230, 230, 230);
            KeysGrid.EnableHeadersVisualStyles = false;
            UpdateKeysList();
        }

        private void refreshBtn_Click(object sender, EventArgs e)
        {
            UpdateKeysList();
        }

        private void newBtn_Click(object sender, EventArgs e)
        {
            NewKey nk = new NewKey();
            nk.Show();
            nk.BringToFront();
            nk.Focus();
        }

        private void KeysGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (KeysGrid.Columns[e.ColumnIndex].Name == "CopyKey")
            {
                System.Windows.Forms.Clipboard.SetText(keys[e.RowIndex].Content);
            }
            if (KeysGrid.Columns[e.ColumnIndex].Name == "CopyPassphrase")
            {
                System.Windows.Forms.Clipboard.SetText(keys[e.RowIndex].Passphrase);
            }
            if (KeysGrid.Columns[e.ColumnIndex].Name == "Delete")
            {
                DialogResult dr = MessageBox.Show("Are you sure you want to delete the key '" + keys[e.RowIndex].Name + "'?", "Delete Key", MessageBoxButtons.YesNo);
                if (dr == DialogResult.Yes)
                {
                    Requests.DeleteKey(keys[e.RowIndex].ID);
                    UpdateKeysList();
                }
            }
        }
    }
}
