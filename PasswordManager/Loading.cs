﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PasswordManager
{
    public partial class Loading : Form
    {
        public bool LoggedIn = false;
        private bool Done = false;
        private bool Success = false;
        private Thread statusThread;

        public Loading()
        {
            InitializeComponent();
        }

        private void ShowMyself()
        {
            this.Visible = true;
            this.Show();
        }

        public void FinishLoad()
        {
            this.Visible = false;
            this.Hide();

            Main main = new Main();
            main.Show();
        }

        private async void Loading_Load(object sender, EventArgs e)
        {
            ShowMyself();
            await Task.Delay(50);

            statusThread = ThreadManager.StartThread(GetStatus);

            while(!Done)
            {
                await Task.Delay(500);
            }

            BasicSettings bs = new BasicSettings();
            if (!Success)
            {
                DialogResult r = MessageBox.Show("Could not connect to the API, please check your settings.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
            }
            else
            {
                bs.Close();
            }

            this.Hide();

            //Everything appears to be working properly on remote, authenticate
            Login lg = new Login();
            lg.Show();
            lg.BringToFront();
            lg.Focus();

            if(!Success)
            {
                bs.Show();
                bs.BringToFront();
                bs.Focus();
            }

            while(!Globals.AUTHENTICATED)
            {
                await Task.Delay(100);
            }

            lg.Hide();

            FinishLoad();
        }

        private void GetStatus()
        {
            _Cfg cfg = new _Cfg();
            for (int i = 0; i < cfg.MAX_TRIES; i++)
            {
                if(!Done)
                {
                    Requests r = new Requests();
                    if (r.Status())
                    {
                        Success = true;
                        Done = true;
                        break;
                    }
                }
            }

            Done = true;

            statusThread.Abort();
        }
    }
}
