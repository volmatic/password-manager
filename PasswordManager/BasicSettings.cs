﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PasswordManager
{
    public partial class BasicSettings : Form
    {
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        private bool Done = false;
        private bool Success = false;
        private Thread statusThread;

        public BasicSettings()
        {
            InitializeComponent();
        }

        private void BasicSettings_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void exitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BasicSettings_Load(object sender, EventArgs e)
        {
            _Cfg cfg = new _Cfg();
            box_api_url.Text = cfg.API_URL;
            box_max_tries.Value = cfg.MAX_TRIES;
            box_web_key.Text = cfg.WEB_KEY;
            box_enc_key.Text = cfg.KEY;
            box_enc_vector.Text = cfg.VECTORS;
        }

        private void WriteConfig()
        {
            string dir = Directory.GetCurrentDirectory() + @"/Config";
            ConfigContents newConfig = new ConfigContents(box_api_url.Text, box_max_tries.Value.ToString(), box_web_key.Text, box_enc_key.Text, box_enc_vector.Text);
            var contents = JsonConvert.SerializeObject(newConfig);
            File.WriteAllText(dir, contents);
        }

        private void GetStatus()
        {
            _Cfg cfg = new _Cfg(box_api_url.Text, Int32.Parse(box_max_tries.Value.ToString()), box_web_key.Text, box_enc_key.Text, box_enc_vector.Text);
            for (int i = 0; i < cfg.MAX_TRIES; i++)
            {
                if (!Done)
                {
                    Requests r = new Requests();
                    if (r.Status(cfg))
                    {
                        Success = true;
                        Done = true;
                        break;
                    }
                }
            }

            Done = true;

            statusThread.Abort();
        }

        private async void testBtn_Click(object sender, EventArgs e)
        {
            Done = false;
            Success = false;
            await Task.Delay(50);

            int keySize = System.Text.ASCIIEncoding.Unicode.GetByteCount(box_enc_key.Text);
            int vecSize = System.Text.ASCIIEncoding.Unicode.GetByteCount(box_enc_vector.Text);
            if (keySize != 32 || vecSize != 32)
            {
                if (keySize != 32)
                {
                    MessageBox.Show("Encryption key is of incorrect length. Expecting 32 but got " + keySize.ToString() + " bytes.", "Invalid Config", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                if (vecSize != 32)
                {
                    MessageBox.Show("IV is of incorrect length. Expecting 32 but got " + keySize.ToString() + " bytes.", "Invalid Config", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                if (box_api_url.Text != "" && box_max_tries.Value > 0 && box_web_key.Text != "")
                {
                    // Try to connect
                    statusThread = ThreadManager.StartThread(GetStatus);

                    while (!Done)
                    {
                        await Task.Delay(500);
                    }

                    if (!Success)
                    {
                        DialogResult r = MessageBox.Show("Could not connect to the API, please try again.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);
                    }
                    else
                    {
                        MessageBox.Show("Successfully connected.");
                    }
                }
                else
                {
                    MessageBox.Show("Invalid configuration, try again.", "Invalid Config", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            WriteConfig();
            this.Close();
        }
    }
}
