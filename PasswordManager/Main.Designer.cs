﻿namespace PasswordManager
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.TopBar = new System.Windows.Forms.Panel();
            this.closeBtn = new System.Windows.Forms.Button();
            this.SideBar = new System.Windows.Forms.Panel();
            this.aboutHref = new System.Windows.Forms.LinkLabel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.keysBtn = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.notesBtn = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.passwordsBtn = new System.Windows.Forms.Button();
            this.Content = new System.Windows.Forms.Panel();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.keys = new PasswordManager.KeysPage();
            this.notes = new PasswordManager.Notes();
            this.passwordPage = new PasswordManager.PasswordPage();
            this.welcomePage = new PasswordManager.WelcomePage();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loginsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.passwordHistoryuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.noteHistoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.keyHistoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TopBar.SuspendLayout();
            this.SideBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.Content.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // TopBar
            // 
            this.TopBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.TopBar.Controls.Add(this.closeBtn);
            this.TopBar.Controls.Add(this.menuStrip1);
            this.TopBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopBar.Location = new System.Drawing.Point(0, 0);
            this.TopBar.Name = "TopBar";
            this.TopBar.Size = new System.Drawing.Size(860, 24);
            this.TopBar.TabIndex = 0;
            this.TopBar.Paint += new System.Windows.Forms.PaintEventHandler(this.TopBar_Paint);
            this.TopBar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            // 
            // closeBtn
            // 
            this.closeBtn.BackColor = System.Drawing.Color.IndianRed;
            this.closeBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.closeBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.closeBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(212)))), ((int)(((byte)(212)))));
            this.closeBtn.Location = new System.Drawing.Point(834, 0);
            this.closeBtn.Name = "closeBtn";
            this.closeBtn.Size = new System.Drawing.Size(23, 23);
            this.closeBtn.TabIndex = 0;
            this.closeBtn.Text = "X";
            this.closeBtn.UseVisualStyleBackColor = false;
            this.closeBtn.Click += new System.EventHandler(this.closeBtn_Click);
            // 
            // SideBar
            // 
            this.SideBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.SideBar.Controls.Add(this.aboutHref);
            this.SideBar.Controls.Add(this.pictureBox3);
            this.SideBar.Controls.Add(this.keysBtn);
            this.SideBar.Controls.Add(this.pictureBox2);
            this.SideBar.Controls.Add(this.notesBtn);
            this.SideBar.Controls.Add(this.pictureBox1);
            this.SideBar.Controls.Add(this.passwordsBtn);
            this.SideBar.Dock = System.Windows.Forms.DockStyle.Left;
            this.SideBar.Location = new System.Drawing.Point(0, 24);
            this.SideBar.Name = "SideBar";
            this.SideBar.Size = new System.Drawing.Size(200, 526);
            this.SideBar.TabIndex = 1;
            this.SideBar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            // 
            // aboutHref
            // 
            this.aboutHref.AutoSize = true;
            this.aboutHref.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(127)))), ((int)(((byte)(116)))));
            this.aboutHref.Location = new System.Drawing.Point(79, 504);
            this.aboutHref.Name = "aboutHref";
            this.aboutHref.Size = new System.Drawing.Size(44, 13);
            this.aboutHref.TabIndex = 4;
            this.aboutHref.TabStop = true;
            this.aboutHref.Text = "About...";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox3.BackgroundImage")));
            this.pictureBox3.Location = new System.Drawing.Point(12, 125);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(25, 25);
            this.pictureBox3.TabIndex = 3;
            this.pictureBox3.TabStop = false;
            // 
            // keysBtn
            // 
            this.keysBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.keysBtn.FlatAppearance.BorderSize = 0;
            this.keysBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.keysBtn.Font = new System.Drawing.Font("News Gothic MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.keysBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(127)))), ((int)(((byte)(116)))));
            this.keysBtn.Location = new System.Drawing.Point(0, 116);
            this.keysBtn.Name = "keysBtn";
            this.keysBtn.Size = new System.Drawing.Size(200, 44);
            this.keysBtn.TabIndex = 4;
            this.keysBtn.Text = "Keys";
            this.keysBtn.UseVisualStyleBackColor = true;
            this.keysBtn.Click += new System.EventHandler(this.keysBtn_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
            this.pictureBox2.Location = new System.Drawing.Point(12, 75);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(25, 25);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // notesBtn
            // 
            this.notesBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.notesBtn.FlatAppearance.BorderSize = 0;
            this.notesBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.notesBtn.Font = new System.Drawing.Font("News Gothic MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.notesBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(127)))), ((int)(((byte)(116)))));
            this.notesBtn.Location = new System.Drawing.Point(0, 66);
            this.notesBtn.Name = "notesBtn";
            this.notesBtn.Size = new System.Drawing.Size(200, 44);
            this.notesBtn.TabIndex = 2;
            this.notesBtn.Text = "Notes";
            this.notesBtn.UseVisualStyleBackColor = true;
            this.notesBtn.Click += new System.EventHandler(this.notesBtn_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox1.Location = new System.Drawing.Point(12, 25);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(25, 25);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // passwordsBtn
            // 
            this.passwordsBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.passwordsBtn.FlatAppearance.BorderSize = 0;
            this.passwordsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.passwordsBtn.Font = new System.Drawing.Font("News Gothic MT", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.passwordsBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(127)))), ((int)(((byte)(116)))));
            this.passwordsBtn.Location = new System.Drawing.Point(0, 16);
            this.passwordsBtn.Name = "passwordsBtn";
            this.passwordsBtn.Size = new System.Drawing.Size(200, 44);
            this.passwordsBtn.TabIndex = 0;
            this.passwordsBtn.Text = "Passwords";
            this.passwordsBtn.UseVisualStyleBackColor = true;
            this.passwordsBtn.Click += new System.EventHandler(this.passwordsBtn_Click);
            // 
            // Content
            // 
            this.Content.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Content.Controls.Add(this.keys);
            this.Content.Controls.Add(this.notes);
            this.Content.Controls.Add(this.passwordPage);
            this.Content.Controls.Add(this.welcomePage);
            this.Content.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Content.Location = new System.Drawing.Point(200, 24);
            this.Content.Name = "Content";
            this.Content.Size = new System.Drawing.Size(660, 526);
            this.Content.TabIndex = 2;
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon1.BalloonTipText = "Password Manager";
            this.notifyIcon1.BalloonTipTitle = "Password Manager is still running. Click here to open it.";
            this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip1;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "Password Manager";
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(119, 48);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.aboutToolStripMenuItem.Text = "About...";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // keys
            // 
            this.keys.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.keys.Dock = System.Windows.Forms.DockStyle.Fill;
            this.keys.Font = new System.Drawing.Font("News Gothic MT", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.keys.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(127)))), ((int)(((byte)(116)))));
            this.keys.Location = new System.Drawing.Point(0, 0);
            this.keys.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.keys.Name = "keys";
            this.keys.Size = new System.Drawing.Size(660, 526);
            this.keys.TabIndex = 3;
            // 
            // notes
            // 
            this.notes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.notes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.notes.Font = new System.Drawing.Font("News Gothic MT", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.notes.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(127)))), ((int)(((byte)(116)))));
            this.notes.Location = new System.Drawing.Point(0, 0);
            this.notes.Margin = new System.Windows.Forms.Padding(4);
            this.notes.Name = "notes";
            this.notes.Size = new System.Drawing.Size(660, 526);
            this.notes.TabIndex = 2;
            // 
            // passwordPage
            // 
            this.passwordPage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.passwordPage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.passwordPage.Font = new System.Drawing.Font("News Gothic MT", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.passwordPage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(127)))), ((int)(((byte)(116)))));
            this.passwordPage.Location = new System.Drawing.Point(0, 0);
            this.passwordPage.Margin = new System.Windows.Forms.Padding(4);
            this.passwordPage.Name = "passwordPage";
            this.passwordPage.Size = new System.Drawing.Size(660, 526);
            this.passwordPage.TabIndex = 1;
            this.passwordPage.Load += new System.EventHandler(this.passwordPage_Load);
            // 
            // welcomePage
            // 
            this.welcomePage.BackColor = System.Drawing.Color.WhiteSmoke;
            this.welcomePage.ForeColor = System.Drawing.Color.White;
            this.welcomePage.Location = new System.Drawing.Point(0, 0);
            this.welcomePage.Name = "welcomePage";
            this.welcomePage.Size = new System.Drawing.Size(650, 526);
            this.welcomePage.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.logToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(860, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem,
            this.logOutToolStripMenuItem,
            this.quitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // logOutToolStripMenuItem
            // 
            this.logOutToolStripMenuItem.Name = "logOutToolStripMenuItem";
            this.logOutToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.logOutToolStripMenuItem.Text = "Log out";
            this.logOutToolStripMenuItem.Click += new System.EventHandler(this.logOutToolStripMenuItem_Click);
            // 
            // quitToolStripMenuItem
            // 
            this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
            this.quitToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.quitToolStripMenuItem.Text = "Quit";
            this.quitToolStripMenuItem.Click += new System.EventHandler(this.quitToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem1});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(45, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem1
            // 
            this.aboutToolStripMenuItem1.Name = "aboutToolStripMenuItem1";
            this.aboutToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.aboutToolStripMenuItem1.Text = "About";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.settingsToolStripMenuItem.Text = "Settings";
            // 
            // logToolStripMenuItem
            // 
            this.logToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showAllToolStripMenuItem,
            this.loginsToolStripMenuItem,
            this.passwordHistoryuToolStripMenuItem,
            this.noteHistoryToolStripMenuItem,
            this.keyHistoryToolStripMenuItem});
            this.logToolStripMenuItem.Name = "logToolStripMenuItem";
            this.logToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.logToolStripMenuItem.Text = "Log";
            // 
            // showAllToolStripMenuItem
            // 
            this.showAllToolStripMenuItem.Name = "showAllToolStripMenuItem";
            this.showAllToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.showAllToolStripMenuItem.Text = "Show All";
            // 
            // loginsToolStripMenuItem
            // 
            this.loginsToolStripMenuItem.Name = "loginsToolStripMenuItem";
            this.loginsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.loginsToolStripMenuItem.Text = "Logins";
            // 
            // passwordHistoryuToolStripMenuItem
            // 
            this.passwordHistoryuToolStripMenuItem.Name = "passwordHistoryuToolStripMenuItem";
            this.passwordHistoryuToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.passwordHistoryuToolStripMenuItem.Text = "Password History";
            this.passwordHistoryuToolStripMenuItem.Click += new System.EventHandler(this.passwordHistoryuToolStripMenuItem_Click);
            // 
            // noteHistoryToolStripMenuItem
            // 
            this.noteHistoryToolStripMenuItem.Name = "noteHistoryToolStripMenuItem";
            this.noteHistoryToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.noteHistoryToolStripMenuItem.Text = "Note History";
            // 
            // keyHistoryToolStripMenuItem
            // 
            this.keyHistoryToolStripMenuItem.Name = "keyHistoryToolStripMenuItem";
            this.keyHistoryToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.keyHistoryToolStripMenuItem.Text = "Key History";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(860, 550);
            this.Controls.Add(this.Content);
            this.Controls.Add(this.SideBar);
            this.Controls.Add(this.TopBar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Main";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Main_Load);
            this.TopBar.ResumeLayout(false);
            this.TopBar.PerformLayout();
            this.SideBar.ResumeLayout(false);
            this.SideBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.Content.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel TopBar;
        private System.Windows.Forms.Panel SideBar;
        private System.Windows.Forms.Panel Content;
        private System.Windows.Forms.Button passwordsBtn;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button notesBtn;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button keysBtn;
        private System.Windows.Forms.Button closeBtn;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private WelcomePage welcomePage;
        private PasswordPage passwordPage;
        private Notes notes;
        private KeysPage keys;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.LinkLabel aboutHref;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logOutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem logToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loginsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem passwordHistoryuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem noteHistoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem keyHistoryToolStripMenuItem;
    }
}

