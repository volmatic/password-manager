﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PasswordManager
{
    public partial class NoteEdit : Form
    {
        public int thisNoteID = -1;
        public string originContents;

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public NoteEdit(int id)
        {
            thisNoteID = id;
            InitializeComponent();
        }

        private void NoteEdit_Load(object sender, EventArgs e)
        {
            if(thisNoteID < 0)
            {
                MessageBox.Show("Something went wrong, please try again later.", "Invalid note", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                _Cfg cfg = new _Cfg();
                string contents = Encryption.Decrypt(Requests.GetNoteContent(thisNoteID), cfg.KEY, cfg.VECTORS);
                originContents = contents;
                noteContents.Text = contents;
            }
        }

        private void NoteEdit_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void closeBtn_Click(object sender, EventArgs e)
        {
            if(originContents != noteContents.Text)
            {
                DialogResult dr = MessageBox.Show("Do you wish to close this window without saving changes?", "Close note editor", MessageBoxButtons.YesNo);
                if (dr == DialogResult.Yes)
                {
                    this.Hide();
                }
            }
            else
            {
                this.Hide();
            }
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            if(noteContents.Text != originContents)
                Requests.SaveNote(thisNoteID, noteContents.Text);
            this.Hide();
        }
    }
}
