﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.Threading;

namespace PasswordManager
{
    public partial class PasswordPage : UserControl
    {
        private static PasswordPage Instance;

        public PasswordPage()
        {
            InitializeComponent();
            pwLength.Text = "16";
            Instance = this;
        }

        private void newPasswordBtn_Click(object sender, EventArgs e)
        {
            NewPassword np = new NewPassword();
            np.Show();
            np.BringToFront();
            np.Focus();
        }

        private void generateBtn_Click(object sender, EventArgs e)
        {
            int length = Int32.Parse(pwLength.Text);
            RNGCryptoServiceProvider cryptRNG = new RNGCryptoServiceProvider();
            byte[] tokenBuffer = new byte[length];
            cryptRNG.GetBytes(tokenBuffer);
            pwBox.Text = Convert.ToBase64String(tokenBuffer);
        }

        private void pwLength_TextChanged(object sender, EventArgs e)
        {

        }
        
        private void PasswordPage_Load(object sender, EventArgs e)
        {
            dataGridView.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(230, 230, 230);
            dataGridView.EnableHeadersVisualStyles = false;
            UpdatePasswordList();
        }

        public Dictionary<int, Password> passwords = new Dictionary<int, Password>();
        public static void UpdatePasswordList()
        {
            int c = 0;
            Instance.passwords.Clear();
            Instance.passwordBindingSource.Clear();
            string dataRaw = Requests.GetPasswords();
            string[] dataArray = dataRaw.Split(new string[] { "<<<|<<!<<<!>>>>!>|>>" }, StringSplitOptions.None);

            foreach (string data in dataArray)
            {
                if (data != "")
                {
                    string[] d0 = data.Split(new string[] { "<o!fSDnmfbnfbds97fgb|!*Ç>" }, StringSplitOptions.None);
                    if (d0.Length != 0)
                    {
                        _Cfg cfg = new _Cfg();
                        string[] visibleData = { Encryption.Decrypt(d0[1], cfg.KEY, cfg.VECTORS), Encryption.Decrypt(d0[2], cfg.KEY, cfg.VECTORS) };
                        string password = Encryption.Decrypt(d0[3], cfg.KEY, cfg.VECTORS);
                        int _id = Int32.Parse(d0[0]);

                        Password np = new Password(_id, visibleData[0], visibleData[1], password);
                        Instance.passwords.Add(c, np);
                        Instance.passwordBindingSource.Add(np);
                        c++;
                    }
                }
            }
        }

        private void dataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if(dataGridView.Columns[e.ColumnIndex].Name == "Copy")
            {
                System.Windows.Forms.Clipboard.SetText(passwords[e.RowIndex].Value);
            }
            if(dataGridView.Columns[e.ColumnIndex].Name == "Delete")
            {
                DialogResult dr = MessageBox.Show("Are you sure you want to delete the password '"+passwords[e.RowIndex].Name+"'?", "Delete Password", MessageBoxButtons.YesNo);
                if (dr == DialogResult.Yes)
                {
                    Requests.DeletePassword(passwords[e.RowIndex].ID);
                    UpdatePasswordList();
                }
            }
            if(dataGridView.Columns[e.ColumnIndex].Name == "Edit")
            {
                if(Globals.CURRENT_EDIT_PASSWORD_ID == -1)
                {
                    Globals.CURRENT_EDIT_PASSWORD_ID = passwords[e.RowIndex].ID;
                    Globals.CURRENT_EDIT_PASSWORD_NUM = e.RowIndex;

                    PasswordEdit pe = new PasswordEdit(this);
                    pe.Show();
                    pe.BringToFront();
                    pe.Focus();
                }
                else
                {
                    MessageBox.Show("You're already editing a password!");
                }
            }
        }

        private void moreBtn_Click(object sender, EventArgs e)
        {
            pwLength.Text = (Int32.Parse(pwLength.Text) + 1).ToString();

            if (Int32.Parse(pwLength.Text) > 128)
                pwLength.Text = "128";
            if (Int32.Parse(pwLength.Text) < 5)
                pwLength.Text = "5";
        }

        private void lessBtn_Click(object sender, EventArgs e)
        {
            pwLength.Text = (Int32.Parse(pwLength.Text) - 1).ToString();

            if (Int32.Parse(pwLength.Text) > 128)
                pwLength.Text = "128";
            if (Int32.Parse(pwLength.Text) < 5)
                pwLength.Text = "5";
        }

        private void pwBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void pwBox_MouseDown(object sender, MouseEventArgs e)
        {
            if(pwBox.Text.Length > 0)
            {
                pwBox.SelectAll();
                System.Windows.Forms.Clipboard.SetText(pwBox.Text);
            }
        }

        private void refreshBtn_Click(object sender, EventArgs e)
        {
            UpdatePasswordList();
        }
    }
}
