﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PasswordManager
{
    public partial class Main : Form
    {
        private bool ballonShown = false;

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public Main()
        {
            InitializeComponent();
            this.ShowInTaskbar = false;
            notifyIcon1.Visible = true;
        }

        private void HideAll()
        {
            passwordPage.Hide();
            notes.Hide();
            keys.Hide();
        }

        private void ShowPasswords()
        {
            HideAll();
            passwordPage.Show();
            passwordPage.BringToFront();
        }

        private void ShowNotes()
        {
            HideAll();
            notes.Show();
            notes.BringToFront();
        }

        private void ShowKeys()
        {
            HideAll();
            keys.Show();
            keys.BringToFront();
        }

        private void Form1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void TopBar_Paint(object sender, PaintEventArgs e)
        {

        }

        private void closeBtn_Click(object sender, EventArgs e)
        {
            //Application.Exit();
            if (this.WindowState == FormWindowState.Normal || this.WindowState == FormWindowState.Maximized)
            {
                Hide();
                this.WindowState = FormWindowState.Minimized;
                if (!ballonShown)
                {
                    notifyIcon1.ShowBalloonTip(2000);
                    ballonShown = true;
                }
                notifyIcon1.Visible = true;
            }
        }
        
        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Show();
            this.WindowState = FormWindowState.Normal;
        }

        private void passwordsBtn_Click(object sender, EventArgs e)
        {
            ShowPasswords();
        }

        private void passwordPage_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void notesBtn_Click(object sender, EventArgs e)
        {
            ShowNotes();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            welcomePage.BringToFront();
        }

        private void keysBtn_Click(object sender, EventArgs e)
        {
            ShowKeys();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void passwordHistoryuToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Globals.AUTHENTICATED = false;
            Login lg = new Login();
            lg.Show();
            lg.BringToFront();
            lg.Focus();
            this.Close();
        }
    }
}
