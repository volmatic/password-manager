﻿namespace PasswordManager
{
    partial class BasicSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.box_api_url = new System.Windows.Forms.TextBox();
            this.box_web_key = new System.Windows.Forms.TextBox();
            this.box_enc_key = new System.Windows.Forms.TextBox();
            this.box_enc_vector = new System.Windows.Forms.TextBox();
            this.saveBtn = new System.Windows.Forms.Button();
            this.testBtn = new System.Windows.Forms.Button();
            this.exitBtn = new System.Windows.Forms.Button();
            this.box_max_tries = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.box_max_tries)).BeginInit();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("News Gothic MT", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(127)))), ((int)(((byte)(116)))));
            this.label3.Location = new System.Drawing.Point(159, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(164, 21);
            this.label3.TabIndex = 5;
            this.label3.Text = "Application Settings";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("News Gothic MT", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(127)))), ((int)(((byte)(116)))));
            this.label1.Location = new System.Drawing.Point(12, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 18);
            this.label1.TabIndex = 6;
            this.label1.Text = "API URL:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("News Gothic MT", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(127)))), ((int)(((byte)(116)))));
            this.label2.Location = new System.Drawing.Point(347, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 18);
            this.label2.TabIndex = 7;
            this.label2.Text = "Max tries:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("News Gothic MT", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(127)))), ((int)(((byte)(116)))));
            this.label4.Location = new System.Drawing.Point(12, 94);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 18);
            this.label4.TabIndex = 8;
            this.label4.Text = "Web Key:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("News Gothic MT", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(127)))), ((int)(((byte)(116)))));
            this.label5.Location = new System.Drawing.Point(12, 124);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 18);
            this.label5.TabIndex = 9;
            this.label5.Text = "Encryption Key:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("News Gothic MT", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(127)))), ((int)(((byte)(116)))));
            this.label6.Location = new System.Drawing.Point(12, 154);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 18);
            this.label6.TabIndex = 10;
            this.label6.Text = "Encription IV:";
            // 
            // box_api_url
            // 
            this.box_api_url.Font = new System.Drawing.Font("News Gothic MT", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.box_api_url.Location = new System.Drawing.Point(77, 60);
            this.box_api_url.Name = "box_api_url";
            this.box_api_url.Size = new System.Drawing.Size(264, 22);
            this.box_api_url.TabIndex = 11;
            // 
            // box_web_key
            // 
            this.box_web_key.Font = new System.Drawing.Font("News Gothic MT", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.box_web_key.Location = new System.Drawing.Point(77, 91);
            this.box_web_key.Name = "box_web_key";
            this.box_web_key.Size = new System.Drawing.Size(370, 22);
            this.box_web_key.TabIndex = 13;
            // 
            // box_enc_key
            // 
            this.box_enc_key.Font = new System.Drawing.Font("News Gothic MT", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.box_enc_key.Location = new System.Drawing.Point(117, 124);
            this.box_enc_key.Name = "box_enc_key";
            this.box_enc_key.Size = new System.Drawing.Size(331, 22);
            this.box_enc_key.TabIndex = 14;
            // 
            // box_enc_vector
            // 
            this.box_enc_vector.Font = new System.Drawing.Font("News Gothic MT", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.box_enc_vector.Location = new System.Drawing.Point(117, 154);
            this.box_enc_vector.Name = "box_enc_vector";
            this.box_enc_vector.Size = new System.Drawing.Size(331, 22);
            this.box_enc_vector.TabIndex = 15;
            // 
            // saveBtn
            // 
            this.saveBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.saveBtn.Font = new System.Drawing.Font("News Gothic MT", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(127)))), ((int)(((byte)(116)))));
            this.saveBtn.Location = new System.Drawing.Point(311, 195);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(137, 30);
            this.saveBtn.TabIndex = 16;
            this.saveBtn.Text = "Save";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // testBtn
            // 
            this.testBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.testBtn.Font = new System.Drawing.Font("News Gothic MT", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.testBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(127)))), ((int)(((byte)(116)))));
            this.testBtn.Location = new System.Drawing.Point(163, 195);
            this.testBtn.Name = "testBtn";
            this.testBtn.Size = new System.Drawing.Size(137, 30);
            this.testBtn.TabIndex = 17;
            this.testBtn.Text = "Test";
            this.testBtn.UseVisualStyleBackColor = true;
            this.testBtn.Click += new System.EventHandler(this.testBtn_Click);
            // 
            // exitBtn
            // 
            this.exitBtn.BackColor = System.Drawing.Color.Transparent;
            this.exitBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.exitBtn.Font = new System.Drawing.Font("News Gothic MT", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitBtn.ForeColor = System.Drawing.Color.IndianRed;
            this.exitBtn.Location = new System.Drawing.Point(12, 195);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(137, 31);
            this.exitBtn.TabIndex = 18;
            this.exitBtn.Text = "Close";
            this.exitBtn.UseVisualStyleBackColor = false;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // box_max_tries
            // 
            this.box_max_tries.Location = new System.Drawing.Point(410, 60);
            this.box_max_tries.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.box_max_tries.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.box_max_tries.Name = "box_max_tries";
            this.box_max_tries.Size = new System.Drawing.Size(38, 23);
            this.box_max_tries.TabIndex = 19;
            this.box_max_tries.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // BasicSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.ClientSize = new System.Drawing.Size(459, 241);
            this.Controls.Add(this.box_max_tries);
            this.Controls.Add(this.exitBtn);
            this.Controls.Add(this.testBtn);
            this.Controls.Add(this.saveBtn);
            this.Controls.Add(this.box_enc_vector);
            this.Controls.Add(this.box_enc_key);
            this.Controls.Add(this.box_web_key);
            this.Controls.Add(this.box_api_url);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Font = new System.Drawing.Font("News Gothic MT", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "BasicSettings";
            this.Text = "BasicSettings";
            this.Load += new System.EventHandler(this.BasicSettings_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.BasicSettings_MouseDown);
            ((System.ComponentModel.ISupportInitialize)(this.box_max_tries)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox box_api_url;
        private System.Windows.Forms.TextBox box_web_key;
        private System.Windows.Forms.TextBox box_enc_key;
        private System.Windows.Forms.TextBox box_enc_vector;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.Button testBtn;
        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.NumericUpDown box_max_tries;
    }
}