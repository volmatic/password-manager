﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PasswordManager
{
    public partial class Notes : UserControl
    {
        private static Notes Instance;

        public Notes()
        {
            InitializeComponent();
            Instance = this;
        }

        private void Notes_Load(object sender, EventArgs e)
        {
            NotesGrid.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(230, 230, 230);
            NotesGrid.EnableHeadersVisualStyles = false;
            UpdateNoteList();
        }

        private Dictionary<int, Note> notes = new Dictionary<int, Note>();
        public static void UpdateNoteList()
        {
            int c = 0;
            Instance.notes.Clear();
            Instance.noteBindingSource.Clear();
            string dataRaw = Requests.GetNotes();
            string[] dataArray = dataRaw.Split(new string[] { "<<<|<<!<<<!>>>>!>|>>" }, StringSplitOptions.None);

            foreach (string data in dataArray)
            {
                if (data != "")
                {
                    string[] d0 = data.Split(new string[] { "<o!fSDnmfbnfbds97fgb|!*Ç>" }, StringSplitOptions.None);
                    if (d0.Length != 0)
                    {
                        _Cfg cfg = new _Cfg();
                        int _id = Int32.Parse(d0[0]);
                        string _name = Encryption.Decrypt(d0[1], cfg.KEY,cfg.VECTORS);
                        string _data = Encryption.Decrypt(d0[2], cfg.KEY, cfg.VECTORS);

                        Note np = new Note(_id, _name, _data);
                        Instance.notes.Add(c, np);
                        Instance.noteBindingSource.Add(np);
                        c++;
                    }
                }
            }
        }

        private void NotesGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void newNoteBtn_Click(object sender, EventArgs e)
        {
            NewNote nn = new NewNote();
            nn.Show();
            nn.BringToFront();
            nn.Focus();
        }

        private void refreshBtn_Click(object sender, EventArgs e)
        {
            UpdateNoteList();
        }

        private void NotesGrid_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            if (NotesGrid.Columns[e.ColumnIndex].Name == "Open")
            {
                NoteEdit ne = new NoteEdit(notes[e.RowIndex].ID);
                ne.Show();
                ne.BringToFront();
                ne.Focus();
            }
            if (NotesGrid.Columns[e.ColumnIndex].Name == "Delete")
            {
                DialogResult dr = MessageBox.Show("Are you sure you want to delete the note '" + notes[e.RowIndex].Name + "'?", "Delete Password", MessageBoxButtons.YesNo);
                if (dr == DialogResult.Yes)
                {
                    Requests.DeleteNote(notes[e.RowIndex].ID);
                    UpdateNoteList();
                }
            }
        }
    }
}
