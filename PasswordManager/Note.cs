﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PasswordManager
{
    public class Note
    {
        public Note(int id, string name, string value)
        {
            this.ID = id;
            this.Name = name;
            this.Value = value;
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
