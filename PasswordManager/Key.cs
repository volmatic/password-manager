﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PasswordManager
{
    public class Key
    {
        public Key(int id, string name, string content, string passphrase)
        {
            this.ID = id;
            this.Name = name;
            this.Content = content;
            this.Passphrase = passphrase;
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public string Passphrase { get; set; }
    }
}
