﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PasswordManager
{
    public class ConfigContents
    {
        public ConfigContents(string _api_url, string _max_tries, string _web_key, string _key, string _vectors)
        {
            this.api_url = _api_url;
            this.max_tries = _max_tries;
            this.web_key = _web_key;
            this.key = _key;
            this.vectors = _vectors;
        }

        public string api_url { get; set; }
        public string max_tries { get; set; }
        public string web_key { get; set; }
        public string key { get; set; }
        public string vectors { get; set; }
    }

    class _Cfg
    {
        public readonly string API_URL = "https://apis.volmatic.xyz/pm/";
        public readonly int MAX_TRIES = 3;
        public readonly string WEB_KEY = "";
        public readonly string KEY = "";
        public readonly string VECTORS = "";

        public _Cfg()
        {
            try
            {
                string dir = Directory.GetCurrentDirectory() + @"/Config";
                ConfigContents c = JsonConvert.DeserializeObject<ConfigContents>(File.ReadAllText(dir));
                this.API_URL = c.api_url;
                this.MAX_TRIES = Int32.Parse(c.max_tries);
                this.WEB_KEY = c.web_key;
                this.KEY = c.key;
                this.VECTORS = c.vectors;
            }
            catch {
            }
            
        }

        public _Cfg(string api_url, int max_tries, string web_key, string key, string vectors)
        {
            this.API_URL = api_url;
            this.MAX_TRIES = max_tries;
            this.WEB_KEY = web_key;
            this.KEY = key;
            this.VECTORS = vectors;
        }
    }
}
