﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace PasswordManager
{
    class StatusRequest
    {
        public string status { get; set; }
    }

    class EmptyRequest
    {
        public string code { get; set; }
    }

    class AuthenticationRequest
    {
        public string code { get; set; }
        public int uid { get; set; }
        public int timestamp { get; set; }
        public string username { get; set; }
    }

    class PasswordListRequest
    {
        public string code { get; set; }
        public string data { get; set; }
    }

    class KeyListRequest
    {
        public string code { get; set; }
        public string data { get; set; }
    }

    class NoteListRequest
    {
        public string code { get; set; }
        public string data { get; set; }
    }

    class Requests
    {
        public bool CreateAccount(string username, string password)
        {
            _Cfg cfg = new _Cfg();
            username = Encryption.Encrypt(username, cfg.KEY, cfg.VECTORS);
            password = Encryption.Encrypt(password, cfg.KEY, cfg.VECTORS);

            WebRequest request = WebRequest.Create(cfg.API_URL + "?a=create&k=" + cfg.WEB_KEY + "&u=" + username + "&p=" + password);

            try
            {
                WebResponse response = request.GetResponse();
                Stream data = response.GetResponseStream();
                string contents = String.Empty;
                using (StreamReader sr = new StreamReader(data))
                {
                    contents = sr.ReadToEnd();
                }

                EmptyRequest jsonContents = JsonConvert.DeserializeObject<EmptyRequest>(contents);
                if (jsonContents.code == "OK")
                    return true;
            }
            catch { return false; }

            return false;
        }

        public static void SaveNote(int noteID, string content)
        {
            _Cfg cfg = new _Cfg();
            content = Encryption.Encrypt(content, cfg.KEY, cfg.VECTORS);

            WebRequest request = WebRequest.Create(cfg.API_URL + "?a=update_note&k=" + cfg.WEB_KEY + "&p=" + Globals.PASSWORD + "&owner=" + Globals.UID + "&noteID=" + noteID + "&content=" + content);

            try
            {
                WebResponse response = request.GetResponse();
                Stream data = response.GetResponseStream();
                string contents = String.Empty;
                using (StreamReader sr = new StreamReader(data))
                {
                    contents = sr.ReadToEnd();
                }
            }
            catch { }
        }

        public static string GetNoteContent(int noteID)
        {
            string answer = "";
            _Cfg cfg = new _Cfg();
            WebRequest request = WebRequest.Create(cfg.API_URL + "?a=note_content&k=" + cfg.WEB_KEY + "&uid=" + Globals.UID + "&p=" + Globals.PASSWORD + "&noteID=" + noteID);
            try
            {
                WebResponse response = request.GetResponse();
                Stream data = response.GetResponseStream();
                string contents = String.Empty;
                using (StreamReader sr = new StreamReader(data))
                {
                    contents = sr.ReadToEnd();
                }

                NoteListRequest jsonContents = JsonConvert.DeserializeObject<NoteListRequest>(contents);

                if (jsonContents.code == "OK")
                {
                    answer = jsonContents.data;
                }
            }
            catch { }

            return answer;
        }

        public static bool ChangePassword(string newPassword)
        {
            _Cfg cfg = new _Cfg();
            newPassword = Encryption.Encrypt(newPassword, cfg.KEY, cfg.VECTORS);
            WebRequest request = WebRequest.Create(cfg.API_URL + "?a=edit_password&k=" + cfg.WEB_KEY + "&p=" + Globals.PASSWORD + "&owner=" + Globals.UID + "&id=" + Globals.CURRENT_EDIT_PASSWORD_ID + "&new=" + newPassword);

            try
            {
                WebResponse response = request.GetResponse();
                Stream data = response.GetResponseStream();
                string contents = String.Empty;
                using (StreamReader sr = new StreamReader(data))
                {
                    contents = sr.ReadToEnd();
                }

                EmptyRequest jsonContents = JsonConvert.DeserializeObject<EmptyRequest>(contents);
                if (jsonContents.code == "OK")
                {
                    return true;
                }
            }
            catch { }

            return false;
        }

        public static void DeleteKey(int id)
        {
            _Cfg cfg = new _Cfg();
            WebRequest request = WebRequest.Create(cfg.API_URL + "?a=delete_key&k=" + cfg.WEB_KEY + "&p=" + Globals.PASSWORD + "&owner=" + Globals.UID + "&id=" + id);

            try
            {
                WebResponse response = request.GetResponse();
                Stream data = response.GetResponseStream();
                string contents = String.Empty;
                using (StreamReader sr = new StreamReader(data))
                {
                    contents = sr.ReadToEnd();
                }
            }
            catch { }
        }

        public static void DeletePassword(int pwID)
        {
            _Cfg cfg = new _Cfg();
            WebRequest request = WebRequest.Create(cfg.API_URL + "?a=delete_password&k=" + cfg.WEB_KEY + "&p=" + Globals.PASSWORD + "&owner=" + Globals.UID + "&pw=" + pwID);

            try
            {
                WebResponse response = request.GetResponse();
                Stream data = response.GetResponseStream();
                string contents = String.Empty;
                using (StreamReader sr = new StreamReader(data))
                {
                    contents = sr.ReadToEnd();
                }
            }
            catch { }
        }

        public static void DeleteNote(int noteID)
        {
            _Cfg cfg = new _Cfg();
            WebRequest request = WebRequest.Create(cfg.API_URL + "?a=delete_note&k=" + cfg.WEB_KEY + "&p=" + Globals.PASSWORD + "&owner=" + Globals.UID + "&noteID=" + noteID);

            try
            {
                WebResponse response = request.GetResponse();
                Stream data = response.GetResponseStream();
                string contents = String.Empty;
                using (StreamReader sr = new StreamReader(data))
                {
                    contents = sr.ReadToEnd();
                }
            }
            catch { }
        }

        public void CreateNewPassword(string name, string desc, string password)
        {
            _Cfg cfg = new _Cfg();

            password = Encryption.Encrypt(password, cfg.KEY, cfg.VECTORS);
            desc = Encryption.Encrypt(desc, cfg.KEY, cfg.VECTORS);
            name = Encryption.Encrypt(name, cfg.KEY, cfg.VECTORS);

            WebRequest request = WebRequest.Create(cfg.API_URL + "?a=new_password&k=" + cfg.WEB_KEY + "&p=" + Globals.PASSWORD + "&owner=" + Globals.UID + "&name=" + name + "&desc=" + desc + "&value=" + password);

            try
            {
                WebResponse response = request.GetResponse();
                Stream data = response.GetResponseStream();
                string contents = String.Empty;
                using (StreamReader sr = new StreamReader(data))
                {
                    contents = sr.ReadToEnd();
                }
            }
            catch { }
        }

        public static void CreateNote(string name)
        {
            _Cfg cfg = new _Cfg();
            name = Encryption.Encrypt(name, cfg.KEY, cfg.VECTORS);
            WebRequest request = WebRequest.Create(cfg.API_URL + "?a=new_note&k=" + cfg.WEB_KEY + "&p=" + Globals.PASSWORD + "&owner=" + Globals.UID + "&name=" + name);

            try
            {
                WebResponse response = request.GetResponse();
                Stream data = response.GetResponseStream();
                string contents = String.Empty;
                using (StreamReader sr = new StreamReader(data))
                {
                    contents = sr.ReadToEnd();
                }
            }
            catch { }
        }

        public static void CreateKey(string name, string key, string pass)
        {
            _Cfg cfg = new _Cfg();

            name = Encryption.Encrypt(name, cfg.KEY, cfg.VECTORS);
            key = Encryption.Encrypt(key, cfg.KEY, cfg.VECTORS);
            pass = Encryption.Encrypt(pass, cfg.KEY, cfg.VECTORS);

            WebRequest request = WebRequest.Create(cfg.API_URL + "?a=new_key&k=" + cfg.WEB_KEY + "&p=" + Globals.PASSWORD + "&owner=" + Globals.UID + "&name=" + name + "&content=" + key + "&pass=" + pass);

            try
            {
                WebResponse response = request.GetResponse();
                Stream data = response.GetResponseStream();
                string contents = String.Empty;
                using (StreamReader sr = new StreamReader(data))
                {
                    contents = sr.ReadToEnd();
                }
            }
            catch { }
        }

        public bool Authenticate(string username, string password)
        {
            _Cfg cfg = new _Cfg();
            username = Encryption.Encrypt(username, cfg.KEY, cfg.VECTORS);
            password = Encryption.Encrypt(password, cfg.KEY, cfg.VECTORS);
            WebRequest request = WebRequest.Create(cfg.API_URL + "?a=auth&k=" + cfg.WEB_KEY + "&u=" + username + "&p=" + password);

            try
            {
                WebResponse response = request.GetResponse();
                Stream data = response.GetResponseStream();
                string contents = String.Empty;
                using (StreamReader sr = new StreamReader(data))
                {
                    contents = sr.ReadToEnd();
                }

                AuthenticationRequest jsonContents = JsonConvert.DeserializeObject<AuthenticationRequest>(contents);
                if (jsonContents.code == "OK")
                {
                    if(jsonContents.uid >= 0 && jsonContents.timestamp != 0 && jsonContents.username != "")
                    {
                        Globals.UID = jsonContents.uid;
                        Globals.AUTH_START = jsonContents.timestamp;
                        Globals.USERNAME = Encryption.Encrypt(jsonContents.username, cfg.KEY, cfg.VECTORS);
                        Globals.PASSWORD = Encryption.Encrypt(password, cfg.KEY, cfg.VECTORS);
                        Globals.AUTHENTICATED = true;
                        return true;
                    }
                }
            }
            catch { return false; }

            return false;
        }

        public static string GetKeys()
        {
            string answer = "";
            _Cfg cfg = new _Cfg();
            WebRequest request = WebRequest.Create(cfg.API_URL + "?a=key_list&k=" + cfg.WEB_KEY + "&uid=" + Globals.UID + "&p=" + Globals.PASSWORD);
            try
            {
                WebResponse response = request.GetResponse();
                Stream data = response.GetResponseStream();
                string contents = String.Empty;
                using (StreamReader sr = new StreamReader(data))
                {
                    contents = sr.ReadToEnd();
                }

                KeyListRequest jsonContents = JsonConvert.DeserializeObject<KeyListRequest>(contents);

                if (jsonContents.code == "OK")
                {
                    answer = jsonContents.data;
                }
            }
            catch { }

            return answer;
        }

        public static string GetNotes()
        {
            string answer = "";
            _Cfg cfg = new _Cfg();
            WebRequest request = WebRequest.Create(cfg.API_URL + "?a=note_list&k=" + cfg.WEB_KEY + "&uid=" + Globals.UID + "&p=" + Globals.PASSWORD);
            try
            {
                WebResponse response = request.GetResponse();
                Stream data = response.GetResponseStream();
                string contents = String.Empty;
                using (StreamReader sr = new StreamReader(data))
                {
                    contents = sr.ReadToEnd();
                }

                NoteListRequest jsonContents = JsonConvert.DeserializeObject<NoteListRequest>(contents);

                if (jsonContents.code == "OK")
                {
                    answer = jsonContents.data;
                }
            }
            catch { }

            return answer;
        }

        public static string GetPasswords()
        {
            string answer = "";
            _Cfg cfg = new _Cfg();
            WebRequest request = WebRequest.Create(cfg.API_URL + "?a=password_list&k=" + cfg.WEB_KEY + "&uid=" + Globals.UID + "&p=" + Globals.PASSWORD);
            try
            {
                WebResponse response = request.GetResponse();
                Stream data = response.GetResponseStream();
                string contents = String.Empty;
                using (StreamReader sr = new StreamReader(data))
                {
                    contents = sr.ReadToEnd();
                }

                PasswordListRequest jsonContents = JsonConvert.DeserializeObject<PasswordListRequest>(contents);

                if (jsonContents.code == "OK")
                {
                    answer = jsonContents.data;
                }
            }
            catch { }

            return answer;
        }

        public bool Status()
        {
            string status = String.Empty;

            _Cfg cfg = new _Cfg();
            WebRequest request = WebRequest.Create(cfg.API_URL + "?a=status&k=" + cfg.WEB_KEY + "&enc_key=" + cfg.KEY + "&enc_vectors=" + cfg.VECTORS);
            try
            {
                WebResponse response = request.GetResponse();
                Stream data = response.GetResponseStream();
                string contents = String.Empty;
                using (StreamReader sr = new StreamReader(data))
                {
                    contents = sr.ReadToEnd();
                }

                StatusRequest jsonContents = JsonConvert.DeserializeObject<StatusRequest>(contents);
                status = jsonContents.status;

                if (status == "OK")
                    return true;
            }
            catch { }

            
           
            return false;
        }

        public bool Status(_Cfg cfg)
        {
            string status = String.Empty;
            WebRequest request = WebRequest.Create(cfg.API_URL + "?a=status&k=" + cfg.WEB_KEY + "&enc_key=" + cfg.KEY + "&enc_vectors=" + cfg.VECTORS);
            try
            {
                WebResponse response = request.GetResponse();
                Stream data = response.GetResponseStream();
                string contents = String.Empty;
                using (StreamReader sr = new StreamReader(data))
                {
                    contents = sr.ReadToEnd();
                }

                StatusRequest jsonContents = JsonConvert.DeserializeObject<StatusRequest>(contents);
                status = jsonContents.status;

                if (status == "OK")
                    return true;
            }
            catch { }



            return false;
        }
    }
}
