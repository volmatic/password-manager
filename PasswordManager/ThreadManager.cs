﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PasswordManager
{
    class ThreadManager
    {
        public static Thread StartThread(Action Method)
        {
            Thread newThread = new Thread(delegate ()
            {
                Method();
            });
            newThread.Start();

            return newThread;
        }
    }
}
