﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PasswordManager
{
    public class Globals
    {
        public static bool AUTHENTICATED;
        public static int UID;
        public static string USERNAME;
        public static int AUTH_START;
        public static string PASSWORD;
        
        public static int CURRENT_EDIT_PASSWORD_ID = -1;
        public static int CURRENT_EDIT_PASSWORD_NUM = -1;
    }
}
